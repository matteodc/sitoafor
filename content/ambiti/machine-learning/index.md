---
title: Machine Learning e Fonti Storiche
summary: Il ruolo del machine learning nella preservazione delle fonti storiche.
authors:
- elia-giacobazzi
tags:
- Machine Learning
- Informatica
date: "2020-06-11T00:00:00Z"

# Optional external URL for project (replaces project detail page).
external_link: ""

image:
  caption: Photo by <a href="https://unsplash.com/@markuswinkler?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText">Markus Winkler</a> on <a href="https://unsplash.com/s/photos/machine-learning?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText">Unsplash</a>
  focal_point: Smart

links:
#- icon: twitter
#  icon_pack: fab
#  name: Follow
#  url: https://twitter.com/georgecushen
url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""

# Slides (optional).
#   Associate this project with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
slides: ""
---

[comment]: <> (Introduzione)
Nel mondo di oggi l’intelligenza artificiale ha un importanza sempre più cruciale e capillare, ma __come può aiutarci a
preservare, analizzare e studiare fonti storiche orali e scritte__?

[comment]: <> (Cos’è il machine learning)
## Cos’è il Machine Learning
Banalizzando all’estremo la questione, un algoritmo di __Machine Learning__ (in italiano _apprendimento automatico_) 
è un algoritmo o _software_ in grado d’imparare (e quindi di migliorarsi) non solo in base alle decisioni di un 
programmatore umano ma anche in base all’esperienza. Normalmente questa "esperienza" viene fornita sotto forma di dati 
o informazioni raccolte e catalogate da esseri umani, ma in alcuni casi imparano direttamente dal contesto in cui agiscono.   

Questo tipo di tecniche ci hanno permesso negli ultimi 70 anni di automatizzare e digitalizzare operazioni un tempo 
realizzabili solo da un essere umano.

Alcuni esempi che riguardano la nostra vita di tutti i giorni possono essere:
* il riconoscimento facciale presente negli _smartphone_ di ultima generazione,
* l’algoritmo che vi suggerisce cosa guardare sul vostro servizio di _streaming_ preferito,
* gli assistenti vocali che gli dici di spegnere il climatizzatore e prontamente ti accendono la luce.

[comment]: <> (Machine Learning e fonti storiche)
## Machine Learning e fonti storiche
Nell’ambito della preservazione dei documenti storici, il _Machine Learning_ può aiutare lo storico o il ricercatore nel 
suo lavoro in vari modi.

### Digitalizzazione
Un documento storico normalmente esiste tramite un supporto fisico (carta, registrazioni analogiche, oggetti, ...), 
questo supporto va incontro a degrado nel tempo. Per poter preservare e rendere accessibile il contenuto un ottima 
strategia è la __digitalizzazione__. Un documento cartaceo può essere scansionato, una video in pellicola può essere 
trasportato in digitale. Questi _file_ possono poi essere __archiviati__ all’interno di un _database_ e visionati 
tramite un sito e/o un portale.

La semplice digitalizzazione però non ci permette di sfruttare a pieno le potenzialità dei sistemi informatici. Se 
scansionassi decine di pagine di giornale e cercassi un particolare trafiletto non potrei fare altro che scorrere ogni 
singola immagine. Molto più interessante invece sarebbe poter cercare i trafiletti scritti da un certo autore e la 
possibilità di poter direttamente copia-incollare il trafiletto. Per questo ci viene incontro la __trascrizione__.

### Trascrizione
Metodi e tecniche per indicizzare contenuti su libri e documenti esistono da secoli, ma ancora oggi buona parte di 
queste operazioni viene eseguita a mano. La trascrizione e l’indicizzazione di un documento storico viene effettuata 
nella maggior parte dei casi da un essere umano, pagina dopo pagina, indice dopo indice. Fino a pochi anni fa non 
c’erano molte alternative, ma la situazione è cambiata.

Aziende come Google, Amazon, Microsoft, da anni propongono soluzioni per trascrivere automaticamente un testo o un audio 
direttamente in un testo digitale, molte stampanti di fascia alta propongono soluzioni OCR per convertire in automatico
la scansione in un documento pdf con testo selezionabile. Perché non applicare queste tecniche a documenti storici?

[comment]: <> (Il problema)
## Il problema
All’interno del progetto AFOr abbiamo applicato tecniche OCR allo stato dell’arte per convertire in automatico 
documenti scansionati in blocchi di testo digitale, in alcuni casi era persino possibile estrapolare dettagli sulla 
struttura della pagina. Purtroppo quasi tutto il testo trascritto era impreciso. L’algoritmo dietro _tesseract_ (la 
libreria OCR che abbiamo utilizzato) tende a digerire male il testo scritto a mano, le imprecisioni della ciclostile e 
le immagini presenti nelle pagine del giornale della Casona o dei volantini delle aziende del villaggio. Questo perché 
nativamente gli algoritmi oggi disponibili sono stati pensati per funzionare su documenti moderni, scritti a computer o 
nel caso delle trascrizioni audio sul parlato moderno... non sul dialetto modenese. 

[comment]: <> (La soluzione)
## Le possibilità
Con lo stato dell’arte di oggi non è possibile digitalizzare in automatico "con un semplice click", serve ancora un 
po’ di lavoro "manuale" per arrivare all’obiettivo, ma questo non vuol dire che siano inutili.

Quello che possiamo fare con queste tecniche è di "fargli fare il grosso del lavoro". Trascrivere centinaia di pagine 
è un lavoro dispendioso, ma correggere una trascrizione approssimativa è comunque un lavoro più gestibile da un essere 
umano.

Possiamo far fare a questi algoritmi una prima parte del lavoro e concentrarci invece sui dettagli. 
Inoltre, come accennato all’inizio, le tecniche di _machine learning_ imparano dall’esperienza. Se il numero di 
documenti è molto alto posso trascrivere manualmente una parte di essi e usare queste informazioni per migliorare 
l’algoritmo stesso e migliorare le sue _performance_.  


Il futuro della storia si prospetta interessante.
