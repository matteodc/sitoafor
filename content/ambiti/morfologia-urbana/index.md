---
title: Morfologia urbana
summary: Studio della forma della città e le sue conseguenti trasformazioni in relazione alle dinamiche antropiche e ambientali che la determinano.
authors: 
- silvia-tagliazucchi
tags:
- Morfologia urbana
date: "2020-06-14T00:00:00Z"

# Optional external URL for project (replaces project detail page).
external_link: ""

image:
  caption: Morfologia urbana del Villaggio Artigiano 
  focal_point: Smart

links:
url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""

# Slides (optional).
#   Associate this project with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
slides: ""
---

La morfologia urbana è un ambito di studio e ricerca solitamente legato all’architettura e alla geografia. Ci sono 
diverse scuole di pensiero a livello mondiale che offrono diverse sfaccettature di questo ambito disciplinare. In AFOr, 
la declinazione di questo ambito è principalmente legato alla scuola italiana, legata alle teorie di morfologia urbana 
e territoriale dell’Architetto __Saverio Muratori__ (Modena, 1910 - Roma, 1973): la forma della città è la conseguenza 
della relazione biunivoca tra uomo e natura, un dialogo costante tra soggetto (uomo) e oggetto (ambiente), tra azione e 
reazione.

In questo dialogo ciclicamente costante tra soggetto e oggetto, le dinamiche con cui l’azione dell’uomo si struttura, 
determinando così la tipologia edilizia, l’impianto e conseguente la forma della città (organismo urbano), il fattore 
antropico si articola e si declina nelle differenti fasi della trasformazione urbana che sono determinate dall’uomo. 
Queste fasi e la loro successione sono determinate da diversi fattori: l’__ambiente__ (fattori ambientali e geologici) 
nel quale viene consolidato l’impianto edilizio, la __comunità__ (fattore sociale e relazionale) che ne costituisce il 
linguaggio architettonico e le sue dinamiche relazionali e la __cultura__ (fattore storico ed estetico) che è 
determinata degli aspetti archetipici su cui si sviluppa la trasformazione dell’area nel tempo.

Questo ambito disciplinare unisce in un dualismo costantemente in dialogo la relazione tra il singolo individuo e lo 
spazio in cui vive; al tempo stesso le dinamiche relazionali della collettività con l’impianto urbano in modo 
totalizzante, conseguentemente articolando questa relazione anche tra gli individui stessi e il modo in cui si 
relazionano tra loro.

In questo la storia e le diverse fasi che le successive trasformazioni determinano giocano un ruolo fondamentale per 
capire le logiche che hanno innescato i cambiamenti dell’area, in una costante alternanza di causa ed effetto, tra 
soggetto e oggetto.

Le voci raccolte in AFOr diventano quindi fondamentali per capire come le relazioni con lo spazio costruito si sono 
articolate nelle diverse fasi della trasformazione del territorio, proponendo la testimonianza del singolo, ma dando 
anche la possibilità attraverso l’ascolto delle diverse storie, di capire e definire i tratti comuni della collettività 
e quindi le dinamiche relazionali che hanno costruito la comunità stessa, ribadendo la relazione tra spazio e corpo.
