---
title: Scansione e stampa 3D degli strumenti dell’artigiano
summary: Dal manuale al digitale. Laboratorio di scansione e stampa 3D degli strumenti dell’artigiano
authors:
- alessandro-zomparelli
tags:
- Stampa 3D
- Scansione 3D  
- Informatica
date: "2020-06-15T00:00:00Z"

# Optional external URL for project (replaces project detail page).
external_link: ""

image:
  name: word-map.jpg
  caption: Strumenti dell’artigiano
  focal_point: Smart

links:
url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""

# Slides (optional).
#   Associate this project with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
slides: ""
---

Elemento fondamentale del lavoro di creazione dell’artigiano sono gli strumenti che esso ha utilizzato, e a volte 
progettato. A integrazione delle video-interviste finora svolte si aggiunge quindi la scansione 3D degli strumenti 
utilizzati dagli artigiani.

La scansione 3D, oltre che documento storico, diventa anche collegamento immaginario tra il lavoro dell’artigiano di 
ieri, con l’artigiano di oggi: il Maker. Egli infatti, avvalendosi degli strumenti digitali e della stampa 3D, 
condivide ed evolve il sapere attraverso le community online.

Attraverso scansione e stampa 3D degli strumenti dell’artigiano, si vuole immaginare un ponte tra gli antichi saperi e 
le comunità online di oggi.

## Video presentazione
{{< youtube LSW7Sbgq4HA >}}


## Laboratorio Festival della Filosofia 2020
{{< youtube LDOYiQXAhVY >}}

## Materiale scansionato
Gli oggetti scansionati in occasione del primo laboratorio sono disponibili sul sito [sketchfab.com](https://sketchfab.com) all'interno di 
questa [collezione](https://sketchfab.com/AFOr/collections/corso-scansione-3d).

<div class="w-100" style="font-size: 0.5rem">
  <iframe width="100%" height="480" frameborder="0" allow="autoplay; fullscreen; vr" allowvr="" 
          src="https://sketchfab.com/playlists/embed?collection=8b67888a32784c9bb720d97d939314fb&autostart=0"
          allowfullscreen="" mozallowfullscreen="true" webkitallowfullscreen="true" onmousewheel=""></iframe>
  <p>
    <a href="https://sketchfab.com/AFOr/collections/corso-scansione-3d" target="_blank" >Corso Scansione 3D</a>
    by <a href="https://sketchfab.com/AFOr" target="_blank">AFOr</a>
    on <a href="https://sketchfab.com?utm_source=website&utm_medium=embed&utm_campaign=share-popup" target="_blank">Sketchfab</a>
  </p>
</div>
