---
title: Linguistica
summary: Il ruolo della linguistica.
tags:
- Linguistica
date: "2020-06-15T00:00:00Z"

# Optional external URL for project (replaces project detail page).
external_link: ""

image:
  caption: Le parole più frequenti nel Giornale della Casona
  focal_point: Smart

links:
url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""

# Slides (optional).
#   Associate this project with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
slides: ""
---

## Laboratorio realizzato per il Festival Filosofia
{{< youtube lC3q0A3cBkQ >}}
