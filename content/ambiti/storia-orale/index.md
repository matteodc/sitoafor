---
title: Storia orale
summary: La registrazione dei ricordi delle persone, trasmessi in una relazione circolare che “provoca” la parola di memoria e la traduce scientificamente in fonte storica.
authors:
- antonio-canovi
tags:
- Storia orale
date: "2020-06-14T00:00:00Z"

# Optional external URL for project (replaces project detail page).
external_link: ""

image:
  caption: Photo by <a href="https://unsplash.com/@imnoom?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText">Noom Peerapong</a> on <a href="https://unsplash.com/s/photos/reel?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText">Unsplash</a>
  focal_point: Smart

links:

url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""

# Slides (optional).
#   Associate this project with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
slides: ""
---

La storia orale è una metodologia storiografica che si fonda sulla creazione e analisi critica di __fonti orali__. Per 
fonte orale s’intende la testimonianza “provocata” con metodo scientifico in uno scambio dialogico liberamente 
contestualizzato tra soggetti e __registrato con cura__ su supporto audio e/o video.

Il __trattamento non formalizzato dell’oralità__ le distingue sia dalla tradizione orale - la quale si occupa di forme 
verbali formalizzate, tramandate, condivise – sia dall’intervista strutturata con questionario - la quale muove alla 
raccolta d’informazioni orali e ne studia le ricorsività. Qui, l’intrinseca ambiguità - agli occhi degli storici 
positivisti - della fonte orale: si dà identità letteraria tra il soggetto del racconto e il protagonista della storia. 
Ma se la storia orale tratta di narrazioni individuali e dei modi e condizioni della loro trasmissione, l’attendibilità 
storica della fonte orale non andrà cercata nella cronologia progressiva dei fatti narrati. __La memoria è atto narrante 
originario e irripetibile__; ogni scarto, ogni silenzio che si produce nel racconto è significativo, cela messaggi 
profondi che richiedono una competenza interpretazione, semantica e storica. La __memoria__, infine, __è una lingua__, 
e __un uomo__, __una donna__ qualsiasi __sono__ anche __testo__.

Lo storico, ma anche l’antropologo, il sociologo, il linguista che avvia scientemente un percorso di ricerca con le 
fonti orali è __consapevole di costruire da sé medesimo le proprie fonti__, come condizione di esistenza. Ciò che 
distingue la fonte orale dalle fonti storiche tradizionali è __l’intenzionalità__: sono documenti in sé, ma non 
preesistono, vanno ricompresi in sede di analisi storiografica quali documenti processuali, generati in un luogo e in 
un tempo dati, come tali preziosissimi per ricostruire metodologicamente un determinato percorso di ricerca. Si tratta 
di una storiografia costruita “con” le memorie restituite dai testimoni della storia. In tal senso, la storia orale è 
una metodologia che presuppone ontologicamente la __partecipazione__ di soggetti esterni al campo della storiografia 
nella stessa costruzione della fonte.

Lo storico orale è il primo archivista delle fonti orali che ha provocato e registrato, ma __costruire un archivio 
orale__ non è solo un dato attinente alle procedure d’indagine. Esso è un __fatto sociale__, cioè produce effetti di 
legittimazione, riconoscimento, valorizzazione, da parte dei soggetti che, con le proprie testimonianze, hanno 
contribuito a produrre quell’archivio. Le storie orali stanno nel respiro della voce e ci parlano fino a che vi siano 
orecchie (le nostre) per intendere. Con AFOR si è inteso, registrando e archiviando a futura memoria, __condividere nel 
tempo presente__ le storie “provocate” grazie al lavoro di rigenerazione che principia dal Villaggio Artigiano Modena 
Ovest. Incontrando persone, famiglie, imprese, associazioni ci siamo proposti di realizzare un ___community archive___, 
dove dare casa a memorie disperse e offrire loro la possibilità di un riscatto rispetto alle narrative dominanti e 
stigmatizzanti. Documentare nel mondo che cambia è una forma di attivismo civile, e AFOR se ne sente partecipe.


