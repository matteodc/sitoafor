---
title: Informatica
summary: Il ruolo dell'informatica.
authors:
- luca-zomparelli
tags:
- Informatica
- Open Source
date: "2020-06-17T00:00:00Z"

# Optional external URL for project (replaces project detail page).
external_link: ""

image:
  caption: Photo by markusspike on Pixabay
  focal_point: Smart

links:
url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""

# Slides (optional).
#   Associate this project with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
slides: ""
---

Dai documenti più semplici agli elaborati più complessi, il contributo dell’informatica è imprescindibile. Che poi in 
un progetto come AFOr, i formati e gli strumenti fossero aperti e liberi, risulta un ulteriore elemento di successo e 
di maggiore fruibilità. Per noi di __ConoscereLinux__, ovviamente, questa è una scelta consapevole e ci ha fatto innamorare 
del progetto nella sua globalità.

Quello che è nato, però, dagli incontri e i confronti relativi ai contenuti e alle possibilità di nuove esplorazioni 
tecnologiche, è stata la voglia di trovare nuove strade e nuovi ambiti, per poter raccontare quelle storie con 
linguaggi nuovi e provando a esaltarne i contenuti.

Dal __machine learning__, alla __scansione e stampa 3D__, fino all’integrazione con altre piattaforme e l’uso di __nuove 
tecnologie__ rappresentative, quello che si è cercato di fare è di aggiungere nuovi elementi a corollario dei racconti 
collezionati nei materiali raccolti.

Ovviamente tutto tramite l’uso di strumenti __OpenSource__, per rendere il progetto e le sue sperimentazioni più 
inclusive, e non inserire così il meno possibile, vincoli tecnologici alla possibilità di provare le stesse 
sperimentazioni su repliche del progetto stesso o di sue parti.

