---
widget: hero
headless: true  # This file represents a page section.
active: true  # Activate this widget? true/false
weight: 1  # Order that this section will appear.
height: 300px

title: "AFOr | Archivio delle Fonti Orali"

# Hero image (optional). Enter filename of an image in the `static/media/` folder.
#hero_media: "va_logo_transparent.png"



design:
  # Apply a background color, gradient, or image.
  #   Uncomment (by removing `#`) an option to apply it.
  #   Choose a light or dark text color by setting `text_color_light`.
  #   Any HTML color name or Hex value is valid.

  # Background color.
  background:
    #color: "navy"

    # Background gradient.
    gradient_start: "#4bb4e3"
    gradient_end: "#2b94c3"
  
    # Background image.
    image: "va_logo_transparent.png"  # Name of image in `static/media/`.
    image_darken: 0.5  # Darken the image? Range 0-1 where 0 is transparent and 1 is opaque.
    image_size: "contain"  #  Options are `cover` (default), `contain`, or `actual` size.
    image_position: "right"  # Options include `left`, `center` (default), or `right`.
    image_parallax: true  # Use a fun parallax-like fixed background effect? true/false

    # Text color (true=light or false=dark).
    text_color_light: false

# Call to action links (optional).
#   Display link(s) by specifying a URL and label below. Icon is optional for `[cta]`.
#   Remove a link/note by deleting a cta/note block.
cta:
  url: "docs/"
  label: "Documentazione tecnica"
  icon_pack: "fas"
  icon: "book"

cta_alt:
#  url: "docs/"
#  label: "View Documentation"

# Note. An optional note to show underneath the links.
cta_note:
#  label: '<span class="js-github-release" data-repo="gcushen/hugo-academic">Show your product version here:<!-- V --></span>'
---

Il progetto __AFOr__ nasce con la volontà di dare una nuova narrazione della Storia 
del __Villaggio Artigiano di Modena Ovest__ attraverso le voci e le testimonianze dei suoi abitanti.
