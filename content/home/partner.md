---
# An instance of the People widget.
# Documentation: https://wowchemy.com/docs/page-builder/
widget: partners

# This file represents a page section.
headless: true

# Order that this section appears on the page.
weight: 26

title: Partner
subtitle: "Le nostre collaborazioni e ibridazioni"

content:
  # Choose which groups/teams of users to display.
  #   Edit `user_groups` in each user's profile to add them to one or more of these groups.
  user_groups:
  - Partner
---

