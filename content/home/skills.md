---
# An instance of the Featurette widget.
# Documentation: https://wowchemy.com/docs/page-builder/
widget: featurette

# This file represents a page section.
headless: true

# Order that this section appears on the page.
weight: 2

title: 
subtitle:

# Showcase personal skills or business features.
# - Add/remove as many `feature` blocks below as you like.
# - For available icons, see: https://wowchemy.com/docs/page-builder/#icons
feature:
- description: Open - libero ed accessibile - nella teoria e nella pratica, tutti i materiali prodotti da AFOr sono condivisi con licenze *open*
  icon: osi
  icon_pack: fab
  name: Aperto
- description: Un approccio partecipato, che comincia con l'ascolto dei protagonisti
  icon: headphones-alt
  icon_pack: fas
  name: Ascolto
- description: I singoli ambiti vengono discussi dai membri per creare nuovo sapere
  icon: people-carry
  icon_pack: fas
  name: Assieme

# Uncomment to use emoji icons.
#- icon: ":smile:"
#  icon_pack: "emoji"
#  name: "Emojiness"
#  description: "100%"  

# Uncomment to use custom SVG icons.
# Place custom SVG icon in `assets/images/icon-pack/`, creating folders if necessary.
# Reference the SVG icon name (without `.svg` extension) in the `icon` field.
#- icon: "your-custom-icon-name"
#  icon_pack: "custom"
#  name: "Surfing"
#  description: "90%"
---
