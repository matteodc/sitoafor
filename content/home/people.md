---
# An instance of the People widget.
# Documentation: https://wowchemy.com/docs/page-builder/
widget: people

# This file represents a page section.
headless: true

# Order that this section appears on the page.
weight: 25

title: Gruppo di lavoro
subtitle: "Chi siamo"

content:
  # Choose which groups/teams of users to display.
  #   Edit `user_groups` in each user's profile to add them to one or more of these groups.
  user_groups:
  - Membro
design:
  show_interests: true
  show_role: true
  show_social: true
---
