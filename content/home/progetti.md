---
widget: blank
headless: true
title: Dialogo con i Progetti
subtitle:
weight: 27 
design:
  # Choose how many columns the section has. Valid values: 1 or 2.
  columns: '1'
---

* [Vides Creaudes](https://videscreuades.com/) | Carpe Studio - Valencia
* [In Loco. Museo diffuso dell’abbandono](https://inloco.eu/) | Spazi Indecisi - Forlì
* [Kiwi. la deliziosa guida di Rosarno](https://www.kiwimemo.it/) | A di Città - Rosarno (RC) e Viaindustriae - Foligno
* [Premio Imagonirmia](https://www.imagonirmia.org/) | Imagonirmia di Elena Mantoni - Treviso
* [Portale dei Saperi](https://www.portaledeisaperi.org/) | Rete italiana della cultura popolare - Torino
