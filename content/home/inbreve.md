---
# Slider widget.
widget: slider
headless: true  # This file represents a page section.
active: true
weight: 10

# Slide interval.
# Use `false` to disable animation or enter a time in ms, e.g. `5000` (5s).
interval: 6000

# Minimum slide height.
# Specify a height to ensure a consistent height for each slide.
height: 400px


item:
  - title: Genesi del progetto AFOr
    content: 'Il progetto AFOR - Archivio delle Fonti Orali nasce nel 2018, grazie al finanziamento della Regione Emilia-Romagna attraverso il Bando Memoria del ‘900, con la volontà ...'
    # Choose `center`, `left`, or `right` alignment.
    align: center
    # Overlay a color or image (optional).
    #   Deactivate an option by commenting out the line, prefixing it with `#`.
    #overlay_color: '#d0ff38'  # An HTML color value.
    overlay_img: pattern_hexagon.jpg  # Image path relative to your `static/media/` folder
    overlay_filter: 0.4  # Darken the image. Value in range 0-1.
    # Call to action button (optional).
    #   Activate the button by specifying a URL and button label below.
    #   Deactivate by commenting out parameters, prefixing lines with `#`.
    cta_label: Leggi le introduzioni
    cta_url: 'inbreve/'
    #cta_icon_pack: fas
    #cta_icon: graduation-cap
  - title: Interviste agli abitanti del Villaggio 
    content: 'una raccolta delle esperienze di chi ha vissuto il villaggio dalla sua nascita e ha contribuito a crearne l‘identità.'
    align: center
    overlay_color: '#555'
    overlay_img: AFOR-interview-01.jpg
    overlay_filter: 0.5
    # cta_label: Vai al materiale su <b>internetarchive.org</b>
    # cta_url: https://archive.org/details/@archiviofontiorali
    cta_label: Guarda le <b>interviste</b>
    cta_url: archive/
---
