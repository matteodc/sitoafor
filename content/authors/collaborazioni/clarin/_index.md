---
# Display name
title: CLARIN-IT

# Set user group
user_groups: ["Collaborazione"]

# Is this the primary user of the site?
superuser: false

link: https://www.clarin-it.it/it

# Highlight the author in author lists? (true/false)
highlight_name: false
---
