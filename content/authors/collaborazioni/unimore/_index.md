---
# Display name
title: UniMORE | Master in Public & Digital History

# Set user group
user_groups: ["Collaborazione"]

# Is this the primary user of the site?
superuser: false

link: https://www.unimore.it/didattica/smaster.html?ID=2244

# Highlight the author in author lists? (true/false)
highlight_name: false
---
