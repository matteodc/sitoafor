---
# Display name
title: Scuola Elementare Emilio Po | Progetto di Storia Locale

# Set user group
user_groups: ["Collaborazione"]

# Is this the primary user of the site?
superuser: false

link: ''

# Highlight the author in author lists? (true/false)
highlight_name: false
---
