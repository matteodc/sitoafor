---
# Display name
title: Istituto d’Arte A. Venturi

# Set user group
user_groups: ["Collaborazione"]

# Is this the primary user of the site?
superuser: false

link: https://www.isarteventuri.edu.it/

# Highlight the author in author lists? (true/false)
highlight_name: false
---
