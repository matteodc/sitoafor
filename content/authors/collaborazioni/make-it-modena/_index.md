---
# Display name
title: Palestra digitale Make-it Modena

# Set user group
user_groups: ["Collaborazione"]

# Is this the primary user of the site?
superuser: false

link: https://www.comune.modena.it/makeitmodena/palestra-digitale

# Highlight the author in author lists? (true/false)
highlight_name: false
---
