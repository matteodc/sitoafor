---
# Display name
title: Progetto Regionale Mappe di Memoria

# Set user group
user_groups: ["Collaborazione"]

# Is this the primary user of the site?
superuser: false

link: https://memorianovecento.emiliaromagnacreativa.it/luoghi/mappe-di-storie-e-memorie/

# Highlight the author in author lists? (true/false)
highlight_name: false
---
