---
# Display name
title: Luca Zomparelli

# Set user group
user_groups: ["Membro"]

# Is this the primary user of the site?
superuser: false

# Role/position/tagline
role: Process Engineer, presidente di ConoscereLinux

# Organizations/Affiliations to show in About widget
organizations:
#- name: Stanford University
#  url: https://www.stanford.edu/

# Short bio (displayed in user profile at end of posts)
bio: 

# Interests to show in About widget
interests:
- Automazione industriale
- Open Source

# Education to show in About widget
education:
  courses:

# Social/Academic Networking
# For available icons, see: https://sourcethemes.com/academic/docs/page-builder/#icons
#   For an email link, use "fas" icon pack, "envelope" icon, and a link in the
#   form "mailto:your-email@example.com" or "/#contact" for contact widget.
social:
- icon: envelope
  icon_pack: fas
  link: '/#contact'
- icon: linkedin
  icon_pack: fab
  link: https://www.linkedin.com/in/lucazomparelli/

# Link to a PDF of your resume/CV.
# To use: copy your resume to `static/media/resume.pdf`, enable `ai` icons in `params.toml`, 
# and uncomment the lines below.
# - icon: cv
#   icon_pack: ai
#   link: media/resume.pdf

# Enter email to display Gravatar (if Gravatar enabled in Config)
email: ""

# Highlight the author in author lists? (true/false)
highlight_name: false
---

Attualmente impiegato in un azienda del comparto automotive, da quasi vent’anni si occupa di automazione industriale, 
con particolare riguardo al testing e all’acquisizione dati. Pur avendo spaziato fra i vari linguaggi di programmazione 
è Certified LabVIEW Developer, da molto tempo appassionato di OpenSource, e da diversi anni innamorato di Python.
Presidente di ConoscereLinux dal 2015
