---
# Display name
title: Tric e Trac Modena

# Set user group
user_groups: ["Partner"]

# Is this the primary user of the site?
superuser: false

link: https://www.facebook.com/tricetracmodena/

# Highlight the author in author lists? (true/false)
highlight_name: false
---
