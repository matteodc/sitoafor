---
# Display name
title: Conoscere Linux

# Set user group
user_groups: ["Partner"]

# Is this the primary user of the site?
superuser: false

link: https://conoscerelinux.org/

# Highlight the author in author lists? (true/false)
highlight_name: false
---
