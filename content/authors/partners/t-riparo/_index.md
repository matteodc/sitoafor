---
# Display name
title: T-Riparo / Officina di comunità

# Set user group
user_groups: ["Partner"]

# Is this the primary user of the site?
superuser: false

link: https://www.facebook.com/t.riparo.officinadicomunita

# Highlight the author in author lists? (true/false)
highlight_name: false
---
