---
# Display name
title: AISO - Associazione Italiana Storia Orale

# Set user group
user_groups: ["Partner"]

# Is this the primary user of the site?
superuser: false

link: https://www.aisoitalia.org/

# Highlight the author in author lists? (true/false)
highlight_name: false
---
