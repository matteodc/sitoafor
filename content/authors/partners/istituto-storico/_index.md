---
# Display name
title: Istituto Storico di Modena

# Set user group
user_groups: ["Partner"]

# Is this the primary user of the site?
superuser: false

link: http://www.istitutostorico.com/

# Highlight the author in author lists? (true/false)
highlight_name: false
---
