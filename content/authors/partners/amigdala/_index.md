---
# Display name
title: Amigdala, gestore della Fabbrica Civica OvestLab

# Set user group
user_groups: ["Partner"]

# Is this the primary user of the site?
superuser: false

link: http://collettivoamigdala.com/

# Highlight the author in author lists? (true/false)
highlight_name: false
---
