---
# Display name
title: Fondazione di Modena

# Set user group
user_groups: ["Patrocinio"]

# Is this the primary user of the site?
superuser: false

link: https://www.fondazionedimodena.it/

# Highlight the author in author lists? (true/false)
highlight_name: false
---
