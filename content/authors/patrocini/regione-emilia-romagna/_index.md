---
# Display name
title: Regione Emilia-Romagna - Settore Cultura

# Set user group
user_groups: ["Patrocinio"]

# Is this the primary user of the site?
superuser: false

link: https://www.regione.emilia-romagna.it/

# Highlight the author in author lists? (true/false)
highlight_name: false
---
