---
# Display name
title: Comune di Modena

# Set user group
user_groups: ["Patrocinio"]

# Is this the primary user of the site?
superuser: false

link: https://www.comune.modena.it/

# Highlight the author in author lists? (true/false)
highlight_name: false
---
