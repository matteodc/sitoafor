---
# Display name
title: Matteo Di Cristofaro

# Set user group
user_groups: ["Membro"]

# Is this the primary user of the site?
superuser: false

# Role/position/tagline
role: Linguista

# Organizations/Affiliations to show in About widget
organizations:
#- name: Stanford University
#  url: https://www.stanford.edu/

# Short bio (displayed in user profile at end of posts)
bio: Lecturer in English Language at Università degli Studi di Modena e Reggio Emilia

# Interests to show in About widget
interests:
- Linguistica Computazionale
- Linguistica Cognitiva

# Education to show in About widget
education:
  courses:

# Social/Academic Networking
# For available icons, see: https://sourcethemes.com/academic/docs/page-builder/#icons
#   For an email link, use "fas" icon pack, "envelope" icon, and a link in the
#   form "mailto:your-email@example.com" or "/#contact" for contact widget.
social:
- icon: envelope
  icon_pack: fas
  link: '/#contact'
#- icon: graduation-cap  # Alternatively, use `google-scholar` icon from `ai` icon pack
#  icon_pack: fas
#  link: https://scholar.google.co.uk/citations?user=sIwtMXoAAAAJ
#- icon: github
#  icon_pack: fab
#  link: https://github.com/gcushen
- icon: linkedin
  icon_pack: fab
  link: https://www.linkedin.com/in/matteo-di-cristofaro/

# Link to a PDF of your resume/CV.
# To use: copy your resume to `static/media/resume.pdf`, enable `ai` icons in `params.toml`, 
# and uncomment the lines below.
# - icon: cv
#   icon_pack: ai
#   link: media/resume.pdf

# Enter email to display Gravatar (if Gravatar enabled in Config)
email: ""

# Highlight the author in author lists? (true/false)
highlight_name: false
---

My fascination with Language and IT led me to become a linguist, specialised in Corpus and Cognitive Linguistics. 
Having worked in several interdisciplinary projects I developed methods and tools to collaborate with different 
disciplines - from the project design to the data analysis, using Free/Libre and Open Source Software (FLOSS) - 
providing a linguistic perspective to better understand the data under investigation.
