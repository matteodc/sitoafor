---
# Display name
title: Alessandro Zomparelli

# Set user group
user_groups: ["Membro"]

# Is this the primary user of the site?
superuser: false

# Role/position/tagline
role: Computational Designer

# Organizations/Affiliations to show in About widget
organizations:
#- name: Stanford University
#  url: https://www.stanford.edu/

# Short bio (displayed in user profile at end of posts)
bio: Computational Designer, professore di tecniche di Modellazione Digitale

# Interests to show in About widget
interests:
- Computational design
- Stampa e scansione 3D

# Education to show in About widget
education:
  courses:

# Social/Academic Networking
# For available icons, see: https://sourcethemes.com/academic/docs/page-builder/#icons
#   For an email link, use "fas" icon pack, "envelope" icon, and a link in the
#   form "mailto:your-email@example.com" or "/#contact" for contact widget.
social:
- icon: envelope
  icon_pack: fas
  link: '/#contact'
- icon: globe
  icon-pack: fas
  link: http://alessandrozomparelli.com/

# Link to a PDF of your resume/CV.
# To use: copy your resume to `static/media/resume.pdf`, enable `ai` icons in `params.toml`, 
# and uncomment the lines below.
# - icon: cv
#   icon_pack: ai
#   link: media/resume.pdf

# Enter email to display Gravatar (if Gravatar enabled in Config)
email: ""

# Highlight the author in author lists? (true/false)
highlight_name: false
---

Computational Designer, membro di MHOX e Co-de-iT
Professore di tecniche di Modellazione Digitale presso l'Accademia di Belle Arti di Bologna.
Svolge progetti e attività didattica riguardo ai temi del computational design e della digital fabrication.

