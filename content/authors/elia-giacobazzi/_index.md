---
# Display name
title: Elia Giacobazzi

# Set user group
user_groups: ["Membro"]

# Is this the primary user of the site?
superuser: false

# Role/position/tagline
role: Specialist of Artificial Intelligence

# Organizations/Affiliations to show in About widget
organizations:
- name: Euler s.r.l.
  url: https://euler.it/

# Short bio (displayed in user profile at end of posts)
bio: Matematico, informatico e imprenditore, con un particolare focus sull'intelligenza artificiale.

# Interests to show in About widget
interests:
- Artificial Intelligence
- Computer Vision
- Information Retrieval

# Education to show in About widget
education:
  courses:
  - course: Master's degree in mathematics
    institution: Università di Modena e Reggio Emilia
    year: 2016

# Social/Academic Networking
# For available icons, see: https://sourcethemes.com/academic/docs/page-builder/#icons
#   For an email link, use "fas" icon pack, "envelope" icon, and a link in the
#   form "mailto:your-email@example.com" or "/#contact" for contact widget.
social:
- icon: envelope
  icon_pack: fas
  link: 'mailto:info@euler.it'
- icon: globe
  icon-pack: fas
  link: https://euler.it
- icon: github
  icon_pack: fab
  link: https://github.com/giobber
- icon: linkedin
  icon_pack: fab
  link: https://www.linkedin.com/in/eliagiacobazzi/

# Link to a PDF of your resume/CV.
# To use: copy your resume to `static/media/resume.pdf`, enable `ai` icons in `params.toml`, 
# and uncomment the lines below.
# - icon: cv
#   icon_pack: ai
#   link: media/resume.pdf

# Enter email to display Gravatar (if Gravatar enabled in Config)
email: ""

# Highlight the author in author lists? (true/false)
highlight_name: false
---
