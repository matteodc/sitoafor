---
title: Archivio
date: 2021-06-21
draft: false
---

Tutto il materiale (interviste, documenti, registrazioni) è disponibile su 
[Internet Archive](https://archive.org/details/@archiviofontiorali) all'indirizzo 
[archive.org/details/@archiviofontiorali](https://archive.org/details/@archiviofontiorali). 

### Video introduttivo

{{< ia-video identifier=afor_trailer >}}

### Le interviste
{{< afor-gallery >}}
