---
title: Genesi del progetto AFOr
date: 2021-06-14
authors:
- silva-tagliazucchi

publication_types: ["0"]
summary: ""
abstract: "Il progetto AFOR - Archivio delle Fonti Orali nasce nel 2018, grazie al finanziamento della Regione Emilia-Romagna attraverso il Bando Memoria del ‘900, con la volontà di dare una nuova narrazione della Storia del villaggio Artigiano di Modena Ovest attraverso le voci e le testimonianze dei suoi abitanti, un racconto corale che possa racchiudere le diverse sfaccettature di vita quotidiana, relazioni e lavoro che hanno caratterizzato quest’area, rappresentativa per tutta la città di Modena."

tags: []
featured: true

---

Il progetto __AFOR__ - __Archivio delle Fonti Orali__ nasce nel 2018, grazie al finanziamento della Regione 
Emilia-Romagna attraverso il [Bando Memoria del ‘900](https://memorianovecento.emiliaromagnacreativa.it/), con la 
volontà di dare una nuova narrazione della __Storia del villaggio Artigiano di Modena Ovest__ attraverso le voci e le 
testimonianze dei suoi abitanti, un racconto corale che possa racchiudere le 
diverse sfaccettature di vita quotidiana, relazioni e lavoro che hanno caratterizzato quest’area, rappresentativa per 
tutta la città di Modena.

Il progetto nasce come sistematizzazione del materiale raccolto da [Amigdala](http://collettivoamigdala.com/) dal 2016 
al Villaggio Artigiano di Modena Ovest attraverso le ricerche svolte sull’area e le interviste ai suoi abitanti, 
prima per la tredicesima edizione del [Festival Periferico](http://collettivoamigdala.com/periferico-festival/) e poi 
nel 2017 per l’avvio alla nuova gestione - insieme ad [Archivio Architetto Cesare Leonardi](http://www.archivioleonardi.it/) - 
della [Fabbrica Civica OvestLab](https://ovestlab.it/), fulcro del progetto di rigenerazione urbana a base culturale 
che l’associazione porta avanti sul quartiere.

Il portato valoriale e la storia stessa che ha dato avvio alla nascita del Villaggio Artigiano di Modena Ovest 
(per approfondimenti sulla storia, vedi pagina dedicata all’identità di OvestLab al seguente 
[link](https://ovestlab.it/identita/)) è stato il motore della ricerca, come ricerca-azione, volta ad approfondire 
la storia di quest’area al fine di contribuire alla creazione di nuovi immaginari futuri per la sua trasformazione e 
ribadirne il valore storico per una nuova narrazione.

Grazie a queste premesse, il progetto ha sviluppato una doppia valenza d’intenti: contribuire ad accrescere la 
conoscenza e la divulgazione della sfaccettata storia della comunità che ha contribuito a creare il Villaggio da una 
parte e al contempo creare nuovi strumenti, soprattutto attraverso la costituzione di un gruppo di lavoro 
interdisciplinare, per mettere in relazione le plurime fonti - le testimonianze, gli utensili e gli attrezzi da lavoro, 
le fotografie, le pubblicazioni … - intercettate durante la ricerca in una logica aperta e volta alla mutua 
collaborazione. Questa vocazione _open source_ ha portato a creare alleanze con altre realtà del territorio e 
collaborazioni a livello nazionale, europeo e internazionale, tanto da immaginare che la metodologia con cui è stato 
creato l’archivio potesse costituire l’apparato tecnico-metodologico per la creazione di una piattaforma che potesse 
essere a disposizione di altri archivi simili riguardanti altre parti della città, con la stessa volontà di riconoscere 
le potenzialità di un territorio attraverso la sua storia e da essa trovare nuovi spunti per immaginare anche il suo 
futuro.
