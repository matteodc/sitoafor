---
title: AFOr, la lingua dell’oralità fuori e dentro l’Archivio

event: Giornata di Studi 2021
event_url: https://www.facebook.com/events/4158078070919110/

location: Dipartimento Studi Linguistici e Culturali
address:
  street: Largo Sant’Eufemia, 19
  city: Modena
  country: Italia
  
summary: Giornata di studi e interazioni transdisciplinari presso il Dipartimento di Studi Linguistici e Culturali, UniMoRe.
abstract: ""

# Talk start and end times.
#   End time can optionally be hidden by prefixing the line with `#`.
date: "2021-06-18T13:00:00Z"
#date_end: "2020-09-20T23:00:00Z"
all_day: true

# Schedule page publish date (NOT talk date).
publishDate: "2017-01-01T00:00:00Z"

authors: []
tags: ["formazione", "ricerca-scientifica"]

# Is this a featured talk? (true/false)
featured: false

image:
  caption: 'Image credit: Gamberipromo'
  focal_point: Right

#links:
#- icon: twitter
#  icon_pack: fab
#  name: Follow
#  url: https://twitter.com/georgecushen
url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""

# Markdown Slides (optional).
#   Associate this talk with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
#slides: example

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects:
- example
---
## Video dell'evento
{{< youtube abL_OuJLuUg >}}

## Programma della giornata

#### Ore 09,00
Saluti di Andrea Bortolamasi, Assessore alla Cultura, Politiche Giovanili e Città Universitaria

### FUORI, si comincia nel paesaggio

#### Ore 09.30-10.30

_Villaggio Modena Ovest: genesi, morfologia e vita di un progetto di rigenerazione urbana_
(Silvia Tagliazucchi - Ass. CivicWise Italia / Ass. Amigdala - Università degli Studi di Ferrara)

_Dall’oralità all’Archivio: la “cassetta degli attrezzi” di un linguista_ 
(Matteo Di Cristofaro - Ass. CivicWise Italia - Università degli Studi di Modena e Reggio Emilia)

_Luogo, archivio, paesaggio: lo storico nelle cerchie della memoria_ 
(Antonio Canovi – Istituto Storico di Modena - Master Digital & Public History UniMoRe)


### DENTRO, si continua nel confronto tra i saperi

#### Ore 10.30 – 12.00

Marina Bondi, linguista - Dipartimento Studi Linguistici e Culturali UniMoRe

Vittorio Iervese, sociologo  – Dipartimento Studi Linguistici Culturali UniMoRe

Manfredi Scanagatta, storico – Master P.H. Dipartimento Studi Linguistici Culturali UniMoRe

### Discussant
#### Ore 12.00-12.45

Francesca Frontini - CNR, Board of Directors CLARIN

Alessandro Casellato, storico – AISO / Università degli Studi Ca’ Foscari

#### Ore 12.45-13.45 Pausa pranzo


### DI NUOVO FUORI, Machine Learning e Memorie Presenti

#### Ore 13.45-16.00 Laboratorio

Intervento interattivo di Elia Giacobazzi, Luca Zomparelli, Alessandro Zomparelli – Ass. ConoscereLinux

Intervento di Federica Rocchi – Ass. Amigdala

Discussione animata da:

Matteo Al Kalak - direttore Centro Interdipartimentale Digital Humanities - DHMoRe

Metella Montanari, Direttore Istituto Storico della Resistenza di Modena

### TRASFERIMENTO

#### Ore 16.00-17.00 - Si va al Villaggio Modena Ovest

OvestLab, Fabbrica civica - 86, Via Nicolò Biondo

#### Ore 17.00

_Archivi, memoria storica e pratiche artistiche. Un dialogo tra pubblico e digitale_
Incontro pubblico a cura di Federica Rocchi (Amigdala)

Interverranno: Isabella Bordoni, Micol Roubini e Archivio Zeta

#### Ore 18,30

A conclusione dell’incontro le parole di Beppe Manni e di Eugenio Ronchetti

#### Ore 18.45

Percorsi da Memorie: partono due geo esplorazioni camminate in contemporanea:

- “Il Villaggio è un Paesaggio” 
- “Il mondo nuovo comincia dalla Macchina”

In contemporanea ai percorsi sarà possibile usufruire dell’aperitivo a cura di Genuino Clandestino