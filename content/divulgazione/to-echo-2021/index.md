---
title: To Echo/Fare Eco
event: Periferico Festival 2021
event_url: 

location: Complesso San Paolo
address:
  street: Via Francesco Selmi, 67
  city: Modena
  country: Italia
  
summary: L’Archivio di Fonti Orali del Villaggio artigiano di Modena Ovest in opera.
abstract: "TO ECHO / FARE ECO è il progetto triennale (2021-2023) di ricerca e residenza artistica basato a Modena che raccoglie intorno ai materiali di AFOr-Archivio delle Fonti Orali del Villaggio Artigiano, le energie e le competenze dell’omonimo gruppo di lavoro, di Collettivo Amigdala/OvestLab e di IMAGONIRMIA art residency & publishing project."

# Talk start and end times.
#   End time can optionally be hidden by prefixing the line with `#`.
date: "2021-11-05T13:00:00Z"
date_end: "2021-11-07T23:00:00Z"
all_day: true

# Schedule page publish date (NOT talk date).
publishDate: "2021-12-08T00:00:00Z"

authors: []
tags: ["collaborazioni-artistiche"]

# Is this a featured talk? (true/false)
featured: false

image:
  caption: 'Image credit: Imagonirmia'
  focal_point: Right

#links:
#- icon: twitter
#  icon_pack: fab
#  name: Follow
#  url: https://twitter.com/georgecushen
url_code: "http://collettivoamigdala.com/portfolio-page/periferico-2021/#1632736173475-9852a767-3228"
url_pdf: ""
url_slides: ""
url_video: ""

# Markdown Slides (optional).
#   Associate this talk with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
#slides: example

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects:
- example
---

**Crediti**

*drammaturgia e trattamento dello spazio* Cinzia Pietribiasi  
*ambienti testuali* Pierluigi Tedeschi  
*immagini* Giuseppe Boiardi  
*ambienti sonori* Alfredo De Vincentiis  
*azioni* Katia Capiluppi, Cinzia Pietribiasi, Pierluigi Tedeschi  
  
*realizzata nell’ambito di* To Echo/Fare Eco – L’archivio di fonti orali del Villaggio artigiano di Modena Ovest in opera  
  
Silvia Tagliazucchi, *responsabile scientifica AFOr*  
Federica Rocchi, *direzione artistica Periferico*  
Isabella Bordoni, *curatela progetto artistico*  

*una produzione di* Amigdala/Periferico Festival, CivicWise Italia e tutto il gruppo AFOr  
*con il sostegno di* Fondazione di Modena – Mi metto all’opera; Regione Emilia Romagna; Comune di Modena  
  
**TO ECHO / FARE ECO** è il progetto triennale (2021-2023) di ricerca e residenza artistica basato a Modena che raccoglie intorno ai materiali di AFOr-Archivio delle Fonti Orali del Villaggio Artigiano, le energie e le competenze dell’omonimo gruppo di lavoro, di Collettivo Amigdala/OvestLab e di IMAGONIRMIA art residency & publishing project.

TO ECHO / FARE ECO rinsalda la collaborazione tra queste tre realtà già connesse su progettualità di lunga durata e dedica la prima residenza a Cinzia Pietribiasi e Pierluigi Tedeschi, che come Compagnia Pietribiasi/Tedeschi firma l’installazione-performance VILLAGGIOARTIGIANO#MEMORIEDELSUOLO.

TO ECHO / FARE ECO prende corpo, estensione, profondità e riabita MOP-Modena Ovest Pavillion, l’edificio in parte abitato e in parte dismesso di Viale Emilio Po che con un processo di ri-significazione concertato tra proprietà, abitanti e soggetti culturali coinvolti, prosegue la scommessa avviata due anni fa, di dare vita a un luogo dell’arte contemporanea nel cuore decentrato, multiculturale e a vocazione ibrida – abitativa e lavorativa – della città. Quel gesto di trasformazione acceso nel 2019 e rimasto vivo anche durante il lockdown del 2020, conserva oggi il suo doppio movimento che da una parte, assumeva alcuni desideri di una comunità abitante al fine di rimettere in vita uno spazio altrimenti chiuso e nascosto, e dall’altra vi faceva convergere formazione, processi e gesti artistici, per riguadagnare una propria presenza urbana attraverso un diverso modo di abitare.

**Compagnia Pietribiasi/Tedeschi** con **VILLAGGIOARTIGIANO#MEMORIEDELSUOLO**, esito della residenza situata nel Villaggio Artigiano Modena Ovest e di cui colloca gli esiti drammaturgici e installativi a MOP, riprende alcune modalità che le sono proprie, come il lavoro intorno alle fonti documentali e la riassunzione della memoria in chiave contemporanea. Nel riabitare con una propria riscrittura artistica gli spazi dell’edificio, VILLAGGIOARTIGIANO#MEMORIEDELSUOLO riaccende la relazione tra l’archivio e il tema dell’abitare, oltre che tra i dati e la loro interpretazione, tra le procedure estrattive e la generazione di senso, e ridisegna la relazione politica e affettiva tra spazio domestico-villaggio artigiano-città-costume-storia collettiva-memoria pubblica.

L’appartamento al secondo piano del 168 di viale Emilio Po, viene interpretato come un organismo vivente: le sue stanze compongono la mappa polisemica e multifunzionale che accoglie e connette le dimensioni privata e pubblica, sia contestualizzando la vita dei singoli nella storia che qui si snoda dal secondo novecento, sia decontestualizzando quelle stesse vite dalla biografia conclusa e finita, per riposizionarle all’interno di un temporalità che ci riguarda. Attraverso i macro temi della casa, della socialità, dei preti operai – movimento che nel secondo dopoguerra congiunge una parte del mondo cattolico al mondo della fabbrica e alla militanza sindacale – e dei movimenti femminili – le diverse esperienze modenesi del neofemminismo dentro e fuori l’UDI, la stagione dei movimenti e dei collettivi delle donne – che proprio al Villaggio Artigiano Modena Ovest hanno avuto alcune delle loro esperienze portanti, **Compagnia Pietribiasi/Tedeschi** con **VILLAGGIOARTIGIANO#MEMORIEDELSUOLO**, riaccende le espressioni della testimonianza e fa della memoria materia viva.

Compagnia Pietribiasi/Tedeschi presenta VILLAGGIOARTIGIANO#MEMORIEDELSUOLO all’interno di Periferico 2021, nell’arco di tre giornate invita il pubblico ad un’esperienza immersiva e dilatata, con attraversamenti degli ambienti e soste che coinvolgono ciascun soggetto come testimone e come corpo esso stesso di memoria.
