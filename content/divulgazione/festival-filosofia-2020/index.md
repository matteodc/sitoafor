---
title: Memorie artigiane, memorie digitali

event: Festival Filosofia 2020
event_url: https://www.festivalfilosofia.it/programma-completo?anno=2020#

location: Complesso San Paolo
address:
  street: Via Francesco Selmi, 67
  city: Modena
  country: Italia
  
summary: Installazione progetto AFOr - Archivio delle Fonti Orali del Villaggio Artigiano di Modena Ovest.
abstract: "Quella che raccontiamo è una storia di macchine costruite a partire da un sito di piccoli capannoni dove lo scambio incessante di invenzioni e saperi artigiani ha dato vita al primo Villaggio Artigiano della città di Modena. Qui, ogni artigiano è stato inventore e fautore di macchine. Le quali, prendendo vita propria, hanno connotato un metodo di produzione e lavoro, uno stile di vita e di relazioni sociali. L’immanenza concreta di questa specifica relazione al senso del vivere ha connotato l’esistenza di questo come il primo Villaggio artigiano della città di Modena."

# Talk start and end times.
#   End time can optionally be hidden by prefixing the line with `#`.
date: "2020-09-18T13:00:00Z"
#date_end: "2020-09-20T23:00:00Z"
all_day: false

# Schedule page publish date (NOT talk date).
publishDate: "2017-01-01T00:00:00Z"

authors: []
tags: ["formazione", "collaborazioni-artistiche", "informatica"]

# Is this a featured talk? (true/false)
featured: false

image:
  caption: 'Image credit: Gamberipromo'
  focal_point: Right

#links:
#- icon: twitter
#  icon_pack: fab
#  name: Follow
#  url: https://twitter.com/georgecushen
url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""

# Markdown Slides (optional).
#   Associate this talk with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
#slides: example

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects:
- example
---

La mostra presenta alcuni degli esiti del progetto di ricerca Archivio delle Fonti Orali del Villaggio Artigiano di 
Modena Ovest, un luogo digitale che raccoglie le memorie orali, scritte, e materiali del quartiere, primo modello di 
questo impianto urbanistico costruito in Italia nel 1953.  Una narrazione corale, nella quale si intrecciano vissuti 
personali e lavorativi. Oltre alla riproduzione delle interviste, nella mostra sarà possibile vedere gli sviluppi che 
la ricerca e le strade perseguite per raccontarla hanno sviluppato: l’applicazione del Machine Learning, della 
scansione e della stampa 3D.
