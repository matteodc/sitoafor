---
title: Paesaggio di Voci
event: Festival Filosofia 2021
event_url: https://www.festivalfilosofia.it/programma-completo?anno=2021#

location: Sala Civica Quartiere 1
address:
  street: Piazza Redecocca, 1
  city: Modena
  country: Italia
  
summary: FestivalFilosofia 2021 | Raccogliere e visualizzare il rapporto tra spazi, memorie, ed emozioni.
abstract: ""

# Talk start and end times.
#   End time can optionally be hidden by prefixing the line with `#`.
date: "2021-09-18T13:00:00Z"
#date_end: "2021-11-07T23:00:00Z"
all_day: true

# Schedule page publish date (NOT talk date).
publishDate: "2021-12-08T00:00:00Z"

authors: []
tags: ["formazione", "collaborazioni-artistiche"]

# Is this a featured talk? (true/false)
featured: false

image:
  caption: 'Image credit: AFOr'
  focal_point: Right

#links:
#- icon: twitter
#  icon_pack: fab
#  name: Follow
#  url: https://twitter.com/georgecushen
url_code: "https://www.festivalfilosofia.it/programma-completo?anno=2021"
url_pdf: ""
url_slides: ""
url_video: ""

# Markdown Slides (optional).
#   Associate this talk with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
#slides: example

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects:
- example
---

Per l’edizione 2021 del FestivalFilosofia, il gruppo di lavoro di AFOr presenta l’installazione interattiva [Paesaggio di voci](https://voci.afor.dev): una pagina web collegata al sito di AFOr che registra posizione e voce all’interno di una mappa interattiva che consente di avere un'immediata percezione dei luoghi e di come vengono percepiti. Nella volontà di dare una declinazione spaziale al tema libertà, il gruppo di AFOr fa una scommessa al pubblico del Festival: partecipare ad un esperimento collettivo di raccolta di testimonianze sul percepito degli spazi in cui avranno luogo le attività del Festival.  
*Come percepisci lo spazio? Come si declina la libertà che hai nel muoverti in questo spazio?*
Attraverso un’applicazione open source accessibile da una semplice pagina web il pubblico del Festival può partecipare all’esperimento, condividendo - sotto forma di tracce digitali testuali e audio - le considerazioni sullo spazio che attraversano.  
L’installazione è ospitata all’interno dello spazio dedicato all’Open Source di [Make It Modena](https://www.comune.modena.it/makeitmodena), dove è data la possibilità di vedere in diretta il cambiamento dei dati e la relazione dei posti visitati dal pubblico stesso in tempo reale.  
  
Durante le giornate del festival sono previsti due appuntamenti divulgativi di spiegazione dell’installazione e di approfondimento anche sul progetto AFOr (la registrazione degli interventi è disponibile qui sotto).  

A cura di: ConoscereLinux, CivicWise Italia, Istituto Storico di Modena


{{< youtube RkLweYywJO0 >}}