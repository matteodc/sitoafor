/** Get JSON data from url */
function fetchJSON(url) {
  return fetch(url).then(response => {
    const contentType = response.headers.get("content-type");
    if (contentType && contentType.indexOf("application/json") !== -1)
      return response.json()
    else
      throw "Not a valid JSON"
  })
}


/** Construct internet archive url based on identifier */
function ia_url(identifier) { return `https://archive.org/details/${identifier}` }
function ia_video_url(identifier) { return `https://archive.org/download/${identifier}/${identifier}.mp4` }


/** Create a custom icon for marker */
function CustomIcon(color, icon, kind='fas') {
  return L.divIcon({
    html: `<div class="custom-icon"><i style="color: ${color}" class="${kind} fa-${icon}"></i></div>`,
    className: ""
  })
}

/** Create a new marker */
function CustomMarker(title, coordinates, url, video_url, color='orange', faIcon='hashtag', faKind='fas') {
  const icon = CustomIcon(color, faIcon, faKind);
  const marker = L.marker(coordinates, { icon: icon });

  let popup = url ? `<p><a href="${url}">${title}</a></p>` : `<p>${title}</p>`;
  if (video_url)
    popup += `
      <video width="320" height="240" controls>
        <source src="${video_url}" type="video/mp4">
      </video>
    `;
  marker.bindPopup(popup);

  return marker;
}

function add_interview(interview, map) {
  const coordinates = [interview.lat, interview.lon];
  const url = ia_url(interview.identifier);
  const video_url = ia_video_url(interview.identifier);

  const marker = CustomMarker(interview.title, coordinates, url, video_url,"black", "film");
  const places = interview.places.map(
    p => CustomMarker(p.title, [p.lat, p.lon], null, null, "darkgreen", "bookmark", "far")
  );
  const lines = places.map(p => L.polyline([coordinates, p.getLatLng()], {color: "gray", opacity: 0}));

  // map.flyToBounds(bounds.extend(coordinates).pad(.5));
  const layer = L.layerGroup().addTo(map);

  marker.addTo(layer);
  places.forEach(p => p.addTo(layer));
  lines.forEach(line => line.addTo(layer));

  function onMouseOver() { lines.forEach(line => line.setStyle({opacity: 0.5})) }
  function onMouseOut() { lines.forEach(line => line.setStyle({opacity: 0})) }

  marker.on("mouseover", onMouseOver).on("mouseout", onMouseOut);
  places.forEach(p => p.on("mouseover", onMouseOver).on("mouseout", onMouseOut));
}


function loadMap(id, dataUrl = "data/map-data.json") {
  // const provider = 'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png';
  const provider = "https://stamen-tiles-{s}.a.ssl.fastly.net/toner-background/{z}/{x}/{y}{r}.png";
  const attribution = ' &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors';
  const center = [44.6543412, 10.9011459];

  // Prepare map
  const map = L.map(id).setView(center, 16);
  L.tileLayer(provider, { attribution: attribution }).addTo(map);

  // OvestLab marker
  const markerOvestlab = CustomMarker("#OvestLab", center, "https://ovestlab.it");
  markerOvestlab.addTo(map);

  const markers = L.featureGroup([markerOvestlab]);
  L.latLngBounds(markers.getBounds());

  // Interview data
  fetchJSON(dataUrl)
    .then(interviews => interviews.forEach(
      (interview, i) => setTimeout(() => add_interview(interview, map), (i+1) * 1000)
    ))
    .catch(error => console.error(error));
}

const mapDataLocation = "data/map-data.json";

const mapContainer = $("#afor-map-container");
if(mapContainer.length)
  document.addEventListener('DOMContentLoaded',() => loadMap("afor-map-container", mapDataLocation));
