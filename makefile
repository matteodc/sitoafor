.PHONY: build
build:
	hugo

.PHONY: server
server:
	hugo server

.PHONY: drafts
drafts:
	hugo server -D


.PHONY: update_interviews
update_interviews:
	curl -o data/interviews.json https://archive.org/advancedsearch.php\?q\=creator%3A%28AFOr+research+group%29+AND+mediatype%3A%28movies%29\&fl%5B%5D\=identifier\&sort%5B%5D\=\&sort%5B%5D\=\&sort%5B%5D\=\&rows\=50\&page\=1\&output\=json\#raw


.PHONY: deploy
deploy: build
	@echo "$(bold)Upload to server$(sgr0)"
	sftp my_webapp@afor.dev:www/ <<< $$'put -r public/*'
